//
//  PostModel.swift
//  africUs
//
//  Created by Handy Metellus on 6/24/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import Foundation

class Post: NSObject {
    
    // Class var
    var id: String!
    var author: String!
    var content: String!
    var excerpt: String!
    var title: String!
    var link: String!
    var featured_image: String!
    var date: String!
    
    // Class var init
    init(post: NSDictionary) {
        self.id = String(describing: post.value(forKey: "id") as! Int)
        //self.author = String(describing: post.value(forKey: "author") as! Int)
        self.content = (post.value(forKey: "content") as? NSDictionary)?.value(forKey: "rendered") as? String
        self.excerpt = (post.value(forKey: "excerpt") as? NSDictionary)?.value(forKey: "rendered") as? String
        self.title = (post.value(forKey: "title") as? NSDictionary)?.value(forKey: "rendered") as? String
        self.featured_image = (post.value(forKey: "better_featured_image") as? NSDictionary)?.value(forKey: "source_url") as? String
        //self.featured_image = ((((post.value(forKey: "better_featured_image") as? NSDictionary)?.value(forKey: "media_details") as? NSDictionary)?.value(forKey: "sizes") as? NSDictionary)?.value(forKey: "medium") as? NSDictionary)?.value(forKey: "source_url") as? String
        self.link = post.value(forKey: "link") as? String
        //self.featured_image = post.value(forKey: "featured_image_thumbnail_url") as? String
        self.date = post.value(forKey: "date") as? String
    }
}
