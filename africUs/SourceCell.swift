//
//  SourceCell.swift
//  africUs
//
//  Created by Andy Metellus on 5/4/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit

class SourceCell: UITableViewCell {
    
    // UI Obj
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var urlTextView: UITextView!
    @IBOutlet weak var separatorView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Align components
        componentAlignmentConfig()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Align components
    func componentAlignmentConfig(){
        
        // Setup contrainst
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        authorLabel.translatesAutoresizingMaskIntoConstraints = false
        urlTextView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        
        // constraints
        self.contentView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-8-[title]-5-[author]-5-[url]-5-[separator(1)]|",
            options: [],
            metrics: nil,
            views: ["title" : titleLabel, "author" : authorLabel, "url" : urlTextView, "separator" : separatorView]))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[title]-10-|",
            options: [],
            metrics: nil,
            views: ["title" : titleLabel]))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[author]-10-|",
            options: [],
            metrics: nil,
            views: ["author" : authorLabel]))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[url]-10-|",
            options: [],
            metrics: nil,
            views: ["url" : urlTextView]))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[separator]|",
            options: [],
            metrics: nil,
            views: ["separator" : separatorView]))
        
        urlTextView.textContainerInset = UIEdgeInsets.zero
        urlTextView.textContainer.lineFragmentPadding = 0
    }

}
