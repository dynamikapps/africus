//
//  ViewController.swift
//  africUs
//
//  Created by Handy Metellus on 5/23/16.
//  Copyright © 2016 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit
import AVFoundation
import ReachabilitySwift
import Alamofire

class FactViewController: UIViewController {
    
    // MARK: - ViewController properties
    
    // UI Objct
    @IBOutlet weak var didYouKnowLabel: UILabel!
    @IBOutlet weak var africUsLogo: UIImageView!
    @IBOutlet weak var factLabel: UILabel!
    @IBOutlet weak var factButton: UIButton!
    @IBOutlet weak var shareBarItem: UIBarButtonItem!
    @IBOutlet weak var moreBarItem: UIBarButtonItem!
    @IBOutlet weak var factBgImageView: UIImageView!
    @IBOutlet weak var shareView: UIView!
    
    // Gradient obj
    let gradientLayer = CAGradientLayer()
    
    // IG controller
    var documentController: UIDocumentInteractionController!
    
    // Assgin nav bgcolor
    //var newColor: UIColor!
    
    // Progress indicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // Stores facts from db
    var factArray: [String] = []
    
    // Saved user index for facts array
    var index =  UserDefaults.standard.integer(forKey: "userFactIndex")
    
    let reachability = Reachability()
    
    var nextFactSound: AVAudioPlayer?
    
    // Declared class property which will store factslist array
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remove back button title
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)

        africUsLogo.isHidden = true
        
        // Set africus logo to navigation title        
        let imageView = UIImageView(image: UIImage(named: "AfricusImageOnly890x"))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 38, height: 38))
        imageView.frame = titleView.bounds
        titleView.addSubview(imageView)
        
        self.navigationItem.titleView = titleView
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
        
        if let nextFactSound = AppSound.setupAudioPlayerWithFile(file: Audio.nextFactSound.audioFileName as NSString, type: Audio.nextFactSound.audioFileType as NSString) {
            self.nextFactSound = nextFactSound
        }
        
        // Setup background gradient layer
        setupGradientLayer()
        
        // config ui
        componentAlignmentConfig()
    }
    
    // MARK: - Start Observer to monitor internet connection
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Disable translucent
        self.navigationController?.navigationBar.isTranslucent = true
        
        // Create nav bar shadow image
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Clear out line under nav bar
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        
        do {
            try reachability?.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
    }
    
    // Align UI components
    func componentAlignmentConfig() {
        
        let width = self.view.frame.size.width
        let height = self.view.frame.size.height
        
        shareView.frame = CGRect(x: width / 18.75,
                                       y: (self.view.center.y - (width / 2)) + (self.navigationController!.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height),
                                       width: width / 1.119,
                                       height: height / 1.99)
        
        // Fact label
        factBgImageView.frame = CGRect(x: shareView.bounds.origin.x,
                                       y: shareView.bounds.origin.y,
                                       width: width / 1.119,
                                       height: height / 1.99)
        
        // Fact label bg image
        factLabel.frame = CGRect(x: factBgImageView.frame.origin.x + (width / 25),
                                 y: factBgImageView.frame.origin.y + (height / 44.4666),
                                 width: factBgImageView.frame.size.width - ((width / 25) * 2),
                                 height: factBgImageView.frame.size.height - ((height / 44.4666) * 2))
        
        // Faded logo for save
        africUsLogo.frame = CGRect(x: self.view.center.x - ((width / 9.375) / 2),
                                   y: shareView.frame.origin.y - ((width / 9.375) + (height / 44.4666)),
                                   width: width / 18.75,
                                   height: width / 18.75)
        // Did you know label
        didYouKnowLabel.frame = CGRect(x: width / 18.75,
                                       y: africUsLogo.frame.origin.y - ((height / 14.822) + (height / 66.7)),
                                       width: width / 1.119,
                                       height: height / 14.822)
        // Next fact button
        factButton.frame = CGRect( x: width / 6.25,
                                   y: shareView.frame.origin.y + shareView.frame.size.height + (height / 23.233),
                                   width: width / 1.47,
                                   height: height / 13.34)
        
        activityIndicator.center.x = africUsLogo.center.x
        activityIndicator.center.y = africUsLogo.center.y
        
    }
    
    // Setup background gradient layer
    func setupGradientLayer() {
        // 1
        self.view.backgroundColor = MyGlobalClass.brandGreenColor
        
        // 2
        gradientLayer.frame = self.view.bounds
        
        // 3
        let color1 = MyGlobalClass.gradientGreenColor.cgColor as CGColor
        let color2 = MyGlobalClass.gradientBlueColor.cgColor as CGColor
        let color3 = MyGlobalClass.gradientIndigoColor.cgColor as CGColor
        gradientLayer.colors = [color1, color2, color3]
        
        // 4
        gradientLayer.locations = [0.0, 0.60, 1.0]
        
        // 5
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        
        // 6
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    // Internet connection changed function
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
                // Load all facts function
                factButton.isEnabled = true
                moreBarItem.isEnabled = true
                shareBarItem.isEnabled = true
                getFacts()
            } else {
                print("Reachable via Cellular")
                // Load all facts function
                factButton.isEnabled = true
                moreBarItem.isEnabled = true
                shareBarItem.isEnabled = true
                getFacts()
            }
        } else {
            print("Network not reachable")
            factButton.isEnabled = false
            moreBarItem.isEnabled = false
            shareBarItem.isEnabled = false
            MyGlobalClass.appDelegate.infoView(message: "No internet connection detected.", color: MyGlobalClass.redColor)
        }
    }
    
    // MARK: Stop Observer once view deinits
    
    deinit {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: reachability)
    }
    
    // Retrive all facts from db
    func getFacts() {
        
        //let myUrl = "http://localhost/africus_app/scripts/facts.php"
        let myUrl = "https://myafricus.com/africus_app/scripts/facts.php"
        
        // Init Progress indicator
        activityIndicator.startAnimating()
        
        //Sending http post request
        Alamofire.request(myUrl, method: .post, parameters: nil).responseJSON
            {
                response in
                //printing response
                //print(response)
                
                switch (response.result) {
                case .success( _):
                    if let result = response.result.value
                    {
                        //converting it as NSDictionary
                        let jsonData = result as? NSDictionary
                        
                        if (jsonData?.value(forKey: "facts") != nil)
                        {
                            // Add facts to dictionary array
                            let facts = jsonData?.value(forKey: "facts") as? [NSDictionary]
                            
                            if let facts = facts {
                                for fact in facts {
                                    
                                    // Parse fact dictionary through fact obj
                                    let factResult = Fact(fact: fact)
                                    self.factArray.append(factResult.fact)
                                }
                                
                                // Stop activity indicator
                                self.activityIndicator.stopAnimating()
                                
                                self.factLabel.text = self.factArray[self.index]
                                self.index += 1
                            }
                        }
                        else if (jsonData?.value(forKey: "message") != nil)
                        {
                            let errorMessage = jsonData?.value(forKey: "message") as? String
                            if let error = errorMessage {
                                MyGlobalClass.appDelegate.infoView(message: error, color: MyGlobalClass.redColor)
                            }
                        }
                    }
                case .failure( _):
                    
                    if let error = response.result.error as? URLError {
                        
                        MyGlobalClass.appDelegate.infoView(message: error.localizedDescription, color: MyGlobalClass.redColor)
                    } else {
                        
                        MyGlobalClass.appDelegate.infoView(message: "Something went wrong, could not connect.", color: MyGlobalClass.redColor)
                    }
                    return
                }
        }
    }
    
    // Display more table view controller
    
    @IBAction func showMoreBarItem(_ sender: Any) {
        if let storyboard = storyboard {
            let VC = storyboard.instantiateViewController(withIdentifier: "MoreTableViewController") as! MoreTableViewController
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    // Display share activity view controller
    @IBAction func shareFactBarItem(_ sender: Any) {
        
        factButton.isHidden = true
        africUsLogo.isHidden = false
        
        var img = UIImage()
        
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: shareView.bounds.size)
            let image = renderer.image { ctx in
                shareView.drawHierarchy(in: shareView.bounds, afterScreenUpdates: true)
            }
            img = image
        } else {
            img =  UIImage.init(view: self.shareView)
        }
        

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Share fact to camera roll
        
        let saveAction = UIAlertAction(title: "Save to Camera Roll", style: .default) {
            (action) in
            
            //let img =  UIImage.init(view: self.shareView)
            UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
            self.factButton.isHidden = false
            self.africUsLogo.isHidden = true
            self.btnAnimation2(sender: self.factButton)
            
        }
        
        // Share fact to instagram
        
        let instagramAction = UIAlertAction(title: "Share via Instagram", style: .default) {
            (action) in
            
            //let img =  UIImage.init(view: self.shareView)
            let num = self.index - 1
            let caption = "\nDid you know?\n\(self.factArray[num]) #africus #history #facts #quotes"
            self.shareToInstagram(img: img, caption: caption)
            self.factButton.isHidden = false
            self.africUsLogo.isHidden = true
            //self.btnAnimation2(sender: self.factButton)
        }
        
        // Share fact to other social media apps
        
        let shareAction = UIAlertAction(title: "Share Via...", style: .default) {
            (action) in
            
            let num = self.index - 1
            //let img =  UIImage.init(view: self.shareView)
            let firstActionItem = img
            let secondActionItem = "\nDid you know?\n\(self.factArray[num]) #africus #history #facts #quotes https://itunes.apple.com/us/app/africus/id1200274542?mt=8"

            let actionViewController = UIActivityViewController(activityItems: [firstActionItem, secondActionItem], applicationActivities: nil)
            self.present(actionViewController, animated: true, completion: nil)
            self.factButton.isHidden = false
            self.africUsLogo.isHidden = true
            //self.btnAnimation2(sender: self.factButton)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            (action) in
            self.factButton.isHidden = false
            self.africUsLogo.isHidden = true
            self.btnAnimation2(sender: self.factButton)
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(instagramAction)
        alertController.addAction(shareAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    // Button action shows next fun fact in firebase array

    @IBAction func showFunFact() {
        
        btnAnimation(sender: factButton)
        playNextFactSound()
        
        if index < factArray.count
        {
            factButton.setImage(UIImage(named: "nextFactButton"), for: .normal)
            factBgImageView.image = BgImageModel().getRandomImage()
            factLabel.text = factArray[index]
            //print("Click index: \(index)")
            //print("Click fact: \(factArray[index])")
            index += 1
            //print("Click next index: \(index)")
            
        }
        else if index == factArray.count
        {
            index = 0
            factButton.setImage(UIImage(named: "startOverFactButton"), for: .normal)
        }
        
        factLabel.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
        factLabel.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0.10, options: .curveLinear, animations: {
            
            self.factLabel.alpha = 1
            self.factLabel.transform = CGAffineTransform.identity
            
        })
    }
    
    // MARK: - Check if app moved to background, saves Fact Number Into database
    
    func appMovedToBackground() {
        
        // Save user info in User object
        UserDefaults.standard.set(index, forKey: "userFactIndex")
        UserDefaults.standard.synchronize()
    }
    
    // MARK: - Animate Button when pressed
    
    func btnAnimation2(sender: UIButton) {
        UIView.animate(withDuration: 0.10 ,
                       animations: {
                        sender.transform = CGAffineTransform(scaleX: 0.50, y: 1.00)
        },
                       completion: { finish in
                        UIView.animate(withDuration: 0.10){
                            sender.transform = CGAffineTransform.identity
                        }
        })
    }
    
    func btnAnimation(sender: UIButton) {
        UIView.animate(withDuration: 0.20 ,
                                   animations: {
                                    sender.transform = CGAffineTransform(scaleX: 0.85, y: 1.00)
        },
                                   completion: { finish in
                                    UIView.animate(withDuration: 0.20){
                                        sender.transform = CGAffineTransform.identity
                                    }
        })
    }
    
    // MARK: - Play Next Sound
    
    func playNextFactSound() {
            nextFactSound?.volume = 0.20
            nextFactSound?.play()
    }
}

    // MARK: - Extension share to Instagram

extension FactViewController: UIDocumentInteractionControllerDelegate {
    
    func shareToInstagram(img: UIImage, caption: String) {
        
        let instagramURL = NSURL(string: "instagram://app")
        //let img =  UIImage.init(view: self.factView)
        
        if (UIApplication.shared.canOpenURL(instagramURL! as URL)) {
            
            let imageData = UIImageJPEGRepresentation(img, 100)
            
            //let captionString = "caption"
            
            let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("instagram.igo")
            //if imageData?.write(to: URL(fileURLWithPath: writePath), options: .atomic) == false
                
            do {
                try imageData?.write(to: URL(fileURLWithPath: writePath), options: .atomic)
                
            } catch let error {
                print(error)
                MyGlobalClass.appDelegate.infoView(message: error as! String, color: MyGlobalClass.redColor)
            }
                
            let fileURL = NSURL(fileURLWithPath: writePath)
            
            self.documentController = UIDocumentInteractionController(url: fileURL as URL)
            
            self.documentController.delegate = self
            
            self.documentController.uti = "com.instagram.exlusivegram"
            
            self.documentController.annotation = NSDictionary(object: caption, forKey: "InstagramCaption" as NSCopying)
            //self.documentController.annotation = ["InstagramCaption": caption]
            self.documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
            
        } else {
            print("Instagram is not installed or app is not allow to open instagram.")
            MyGlobalClass.appDelegate.infoView(message: "Instagram is not installed or app is not allow to open instagram.", color: MyGlobalClass.redColor)
        }
    }
}

    // MARK: - Extension Take a Screenshot of Fact

extension UIImage{
    convenience init(view: UIView) {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
}

