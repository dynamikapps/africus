//
//  FeedbackViewController.swift
//  africUs
//
//  Created by Andy Metellus on 5/1/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Alamofire

class FeedbackViewController: UIViewController {

    // UI Obj
    @IBOutlet weak var selectLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var selectTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    // Progress indicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // Picker values
    var options: [String]!
    // Picker object
    var suggestPickerView: UIPickerView!
    
    let reachability = Reachability()
    
    var keyboard = CGRect()
    
    // Textview placeholder
    lazy var placeHolderLabelText: UILabel = {
        
        let placeholderLabel = UILabel()
        placeholderLabel.text = "Add a message"
        placeholderLabel.sizeToFit()
        placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
        
        return placeholderLabel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // config ui
        componentAlignmentConfig()
        
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeedbackViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // PickerView Programmatically
        suggestPickerView = UIPickerView()
        //genderPickerView.frame = CGRect(x: 0, y: 300, width: self.view.bounds.width, height: 200)
        suggestPickerView.showsSelectionIndicator = true
        suggestPickerView.backgroundColor = MyGlobalClass.brandGreenColor
        suggestPickerView.delegate = self
        suggestPickerView.dataSource = self
        
        // array of option strings
        options = ["Suggest a Fact", "Support Issue"]
        
        // allows us to call coolPickerView ONLY when we select TextField
        selectTextField.inputView = suggestPickerView
        
        // Set textfield tintcolor
        emailTextField.tintColor = MyGlobalClass.brandGreenColor
        nameTextField.tintColor = MyGlobalClass.brandGreenColor
        messageTextView.tintColor = MyGlobalClass.brandGreenColor
        selectTextField.tintColor = UIColor.clear
        
        //Sets default gender string value
        suggestPickerView.selectRow(0, inComponent: 0, animated: true)

        // Keyboard notification
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: - Start Observer to monitor internet connection
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        
        do {
            try reachability?.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
    }
    
    // Internet connection changed function
    func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
                // Load all facts function
                sendButton.isEnabled = true
                resetButton.isEnabled = true
            } else {
                print("Reachable via Cellular")
                // Load all facts function
                sendButton.isEnabled = true
                resetButton.isEnabled = true
            }
        } else {
            print("Network not reachable")
            sendButton.isEnabled = false
            resetButton.isEnabled = false
            MyGlobalClass.appDelegate.infoView(message: "No internet connection detected.", color: MyGlobalClass.redColor)
        }
    }
    
    // MARK: Stop Observer once view deinits
    
    deinit {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: reachability)
    }
    
    // config ui
    func componentAlignmentConfig() {
        
        let width = self.view.frame.size.width
        let height = self.view.frame.size.height
        
        scrollView.frame = CGRect(x: 0,
                                  y: 0,
                                  width: width,
                                  height: height)
        
        selectLabel.frame = CGRect(x: width / 25,
                                   y: width / 25,
                                   width: width - ((width / 25) * 2),
                                   height: 20)
        
        selectTextField.frame = CGRect(x: selectLabel.frame.origin.x,
                                       y: selectLabel.frame.origin.y + selectLabel.frame.size.height + (width / 37.5),
                                       width: width - ((width / 25) * 2),
                                       height: 30)
        
        nameLabel.frame = CGRect(x: selectLabel.frame.origin.x,
                                 y: selectTextField.frame.origin.y + selectTextField.frame.size.height + (width / 25),
                                 width: width - ((width / 25) * 2),
                                 height: 20)
        
        nameTextField.frame = CGRect(x: selectLabel.frame.origin.x,
                                     y: nameLabel.frame.origin.y + nameLabel.frame.size.height + (width / 37.5),
                                     width: width - ((width / 25) * 2),
                                     height: 30)
        
        emailLabel.frame = CGRect(x: selectLabel.frame.origin.x,
                                  y: nameTextField.frame.origin.y + nameTextField.frame.size.height + (width / 25),
                                  width: width - ((width / 25) * 2),
                                  height: 20)
        
        emailTextField.frame = CGRect(x: selectLabel.frame.origin.x,
                                      y: emailLabel.frame.origin.y + emailLabel.frame.size.height + (width / 37.5),
                                      width: width - ((width / 25) * 2),
                                      height: 30)
        
        messageLabel.frame = CGRect(x: selectLabel.frame.origin.x,
                                    y: emailTextField.frame.origin.y + emailTextField.frame.size.height + (width / 25),
                                    width: width - ((width / 25) * 2),
                                    height: 20)
        
        messageTextView.frame = CGRect(x: selectLabel.frame.origin.x,
                                       y: messageLabel.frame.origin.y + messageLabel.frame.size.height + (width / 37.5),
                                       width: width - ((width / 25) * 2),
                                       height: width / 4.6875)
        
        sendButton.frame = CGRect(x: selectLabel.frame.origin.x,
                                  y: messageTextView.frame.origin.y + messageTextView.frame.size.height + (width / 25),
                                  width: width / 3,
                                  height: 30)
        
        resetButton.frame = CGRect(x: (width / 25) + sendButton.frame.size.width + (width / 3.9473),
                                   y: messageTextView.frame.origin.y + messageTextView.frame.size.height + (width / 25),
                                   width: width / 3,
                                   height: 30)
        
        // Rounded corners
        sendButton.layer.cornerRadius = 6
        sendButton.clipsToBounds = true
        resetButton.layer.cornerRadius = 6
        resetButton.clipsToBounds = true
        
        // Border and curved bioview
        messageTextView.layer.cornerRadius = 6
        messageTextView.clipsToBounds = true
        messageTextView.layer.borderWidth = 1
        messageTextView.layer.masksToBounds = true
        messageTextView.layer.borderColor = MyGlobalClass.tableGrayColor.cgColor
        
        // Configures placerholder in textview
        self.placeHolderLabelText.font = UIFont.systemFont(ofSize: self.messageTextView.font!.pointSize)
        self.messageTextView.addSubview(self.placeHolderLabelText)
        self.placeHolderLabelText.frame.origin = CGPoint(x: width / 75, y: self.messageTextView.font!.pointSize / 2)
        self.placeHolderLabelText.isHidden = !self.messageTextView.text.isEmpty
    }
    
    // Adjust view when keyboard shows
    func keyboardWillShow(notification: NSNotification) {
        
        // Define keyboard size
        keyboard = ((notification.userInfo?[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue)
        
        // Move UI up animation
        UIView.animate(withDuration: 0.4) {
            self.scrollView.contentSize.height = self.scrollView.frame.size.height + self.keyboard.height / 2
        }
    }
    
    // Adjust view when keyboard hides
    func keyboardWillHide(notification: NSNotification) {
        
        // Move UI down animation
        UIView.animate(withDuration: 0.4) {
            self.scrollView.contentSize.height = 0
        }
    }
    
    // Dismiss VC
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // Send button tapped
    @IBAction func sendButtonTapped(_ sender: Any) {
        
        guard let email = emailTextField.text, let name = nameTextField.text, let message = messageTextView.text, let selected = selectTextField.text, !selected.isEmpty && !email.isEmpty && !name.isEmpty && !message.isEmpty else {
            
            // red placeholders
            emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName: MyGlobalClass.redColor])
            nameTextField.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSForegroundColorAttributeName: MyGlobalClass.redColor])
            selectTextField.attributedPlaceholder = NSAttributedString(string: "Select", attributes: [NSForegroundColorAttributeName: MyGlobalClass.redColor])
            placeHolderLabelText.text = "Message"
            placeHolderLabelText.textColor = MyGlobalClass.redColor
            
            MyGlobalClass.appDelegate.infoView(message: "All fields are required!", color: MyGlobalClass.redColor)
            
            return
        }
        
        // Verify name count is correct
        if name.trim().characters.count < 3 || name.trim().characters.count > 30 {
            
            MyGlobalClass.appDelegate.infoView(message: "Name must have at least 4 but no more than 30 chars. You have \(name.trim().characters.count) chars.", color: MyGlobalClass.redColor)
            return
        }
        
        // Verify email format is correct
        if isValidEmail(testStr: email.trim()) == false {
            
            MyGlobalClass.appDelegate.infoView(message: "Please enter a valid email address.", color: MyGlobalClass.redColor)
            return
        }
        
        // Verify message count is correct
        if message.trim().characters.count < 9 {
            
            MyGlobalClass.appDelegate.infoView(message: "Message must have at least 10 chars. You have \(message.trim().characters.count) chars.", color: MyGlobalClass.redColor)
            return
        }
        
        // Create complaint uuid
        let uuid = NSUUID().uuidString
        
        //let myUrl = "https://localhost/africus_app/scripts/feedback.php"
        let myUrl = "https://myafricus.com/africus_app/scripts/feedback.php"
        
        // Assign selected type
        var type = "none"
        
        if selected == "Suggest a Fact" {
            type = "suggestion"
        } else if selected == "Support Issue" {
            type = "support"
        } else {
            type = "suggestion"
        }
        
        //creating parameters for the post request
        let parameters: Parameters = [
            "uuid": uuid,
            "email": email,
            "name": name,
            "type": type,
            "text": message
        ]
        
        // Hide Keyboard
        self.view.endEditing(true)
        
        // Progress indicator
        activityIndicator.startAnimating()
        
        //Sending http post request
        Alamofire.request(myUrl, method: .post, parameters: parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                switch (response.result) {
                    
                case .success( _):
                    // Stop activity indicator
                    self.activityIndicator.stopAnimating()
                    if let result = response.result.value
                    {
                        //converting it as NSDictionary
                        let jsonData = result as? NSDictionary
                        
                        if let message = jsonData?.value(forKey: "message") as? String {
                            
                            MyGlobalClass.appDelegate.infoView(message: message, color: MyGlobalClass.redColor)
                            print(message)
                        } else {
                            
                            // Create alert
                            let alertController = UIAlertController(title: name, message: "Thank you for your contribution.", preferredStyle: .alert)
                            self.present(alertController, animated: true, completion: nil)
                            
                            let okAction = UIAlertAction(title: "Ok", style: .default) {
                                
                                (action) in
                                
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(okAction)
                        }
                    }
                case .failure( _):
                    // Stop activity indicator
                    self.activityIndicator.stopAnimating()
                    if let error = response.result.error as? URLError {
                        MyGlobalClass.appDelegate.infoView(message: error.localizedDescription, color: MyGlobalClass.redColor)
                    } else {
                        MyGlobalClass.appDelegate.infoView(message: "Something went wrong, could not connect to server.", color: MyGlobalClass.redColor)
                    }
                    return
                }
        }
    }
    
    // Reset button tapped
    @IBAction func resetButtonTapped(_ sender: Any) {
        emailTextField.text = ""
        messageTextView.text = ""
        nameTextField.text = ""
    }
    
    // Hide keyboard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK: - Check if text is the correct email format
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension FeedbackViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    // total numb of components - Vertical
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // total numb of rows - Horizontal
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return options.count
    }
    
    // title for PickerView
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return options[row]
    }
    
    // width of component - Vertical
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return self.view.bounds.width
    }
    
    // height of component - Horizontal
    /*func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
     return 50
     }*/
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var attributedString: NSAttributedString!
        
        switch component {
        case 0:
            attributedString = NSAttributedString(string: options[row], attributes: [NSForegroundColorAttributeName : UIColor.white])
        case 1:
            attributedString = NSAttributedString(string: options[row], attributes: [NSForegroundColorAttributeName : UIColor.white])
        default:
            attributedString = nil
        }
        
        return attributedString
    }
    
    // called when did select some row
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        // assign certain option to textField's text
        selectTextField.text = options[row]
        
        // dismiss keyboard
        selectTextField.resignFirstResponder()
    }
}

extension FeedbackViewController: UITextFieldDelegate {
    
    // Send user to email or phone view to edit
    /*func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == selectTextField {
            self.view.endEditing(true)
        }
    }*/
    
    /*func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return false
    }*/
}

extension FeedbackViewController: UITextViewDelegate {
    
    // MARK: - Textview did change observer
    
    func textViewDidChange(_ textView: UITextView) {
        
        // Hide placeholder if textview is not empty
        self.placeHolderLabelText.isHidden = !textView.text.isEmpty
        
        // Set placeholder color back to gray
        placeHolderLabelText.text = "Add a message"
        placeHolderLabelText.textColor = UIColor(white: 0, alpha: 0.3)
        
        /*if self.inputTextField.text!.isEmpty || Reachability.isConnectedToNetwork() == false {
            self.sendButton.backgroundColor = UIColor.lightGrayColor()
            self.sendButton.enabled = false
        } else {
            self.sendButton.enabled = true
            self.sendButton.backgroundColor = ColorComponents.RGB(red: 52, green: 113, blue: 157, alpha: 1).color()
        }*/
    }
    
    // MARK: - Check for user space or blanks
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        /*if text == "\n"
         {
         textView.resignFirstResponder()
         return false
         }*/
        let maxLength = 200
        let currentString: NSString = textView.text as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
        //self.charCountLabelText.text = "\(maxLength - newString.length)"
        //print(maxLength - newString.length)
        return newString.length < maxLength
    }
}

