//
//  AppSoundModel.swift
//  africUs
//
//  Created by Handy Metellus on 1/24/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

enum Audio {
    case nextFactSound
    
    var audioFileName: String {
        switch self {
        case .nextFactSound: return "nextFactSound"
        }
    }
    var audioFileType: String {
        switch self {
        case .nextFactSound: return "wav"
        }
    }
}

class AppSound {
    
    class func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer?  {
        //1 takes in file name and type path to return audio file
        let path = Bundle.main.path(forResource: file as String, ofType: type as String)
        let url = NSURL.fileURL(withPath: path!)
        
        //2 Create audio player object
        var audioPlayer: AVAudioPlayer?
        
        // Preperation
        //try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient, withOptions: .DuckOthers)
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        try! AVAudioSession.sharedInstance().setActive(true)
        
        // 3 check if audio is avlb and create object or returns null
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
}
