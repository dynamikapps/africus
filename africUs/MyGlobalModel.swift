//
//  MyGlobalModel.swift
//  africUs
//
//  Created by Andy Metellus on 4/26/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import Foundation
import UIKit

class MyGlobalClass {
    
    // Global variable referred to AppDelegate to call from any class
    static let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Global info view colors
    // let redColor = ColorComponents.RGB(red: 255, green: 50, blue: 75, alpha: 1).color()
    static let redColor = UIColor(red: 252/255, green: 68/255, blue: 15/255, alpha: 1)
    
    static let greenColor = UIColor(red: 180/255, green: 227/255, blue: 61/255, alpha: 1)
    
    static let blueColor = UIColor(red: 35/255, green: 46/255, blue: 209/255, alpha: 1)
    
    static let tableGrayColor = UIColor(red: 235/255, green: 235/255, blue: 241/255, alpha: 1)
    
    static let brandGreenColor = UIColor(red: 90/255, green: 187/255, blue: 181/255, alpha: 1)
    
    static let gradientGreenColor = UIColor(red: 180/255, green: 236/255, blue: 81/255, alpha: 0.35)
    
    static let gradientBlueColor = UIColor(red: 83/255, green: 160/255, blue: 253/255, alpha: 0.35)
    
    static let gradientIndigoColor = UIColor(red: 48/255, green: 35/255, blue: 174/255, alpha: 0.35)
    
    // Globel info view label sizse
    static let fontSize12 = UIScreen.main.bounds.width / 31
    
    static let htmlStyleBegin = "<html><head><meta charset=\"UTF-8\"><style> body { color: #555555; font-family:Helvetica; font-size: 14px } h1 { color: #ffffff; font-family:Chalkduster; font-size: 18pt } a { font-family:Helvetica; font-weight: bold; color: #5ABBB5; text-decoration:none; padding-left: 5px; } img { padding-left:0; align: center; max-width: 100%; max-height: 100%} iframe { padding-left:0; align: center; max-width: 100%; max-height: 100%} .content { padding-left: 1em; padding-top: 1em; }</style></head><body>"
    
    static let htmlStyleEnd = "</body></html>"
}

// MARK: - Extension to Trim Input Text

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}
