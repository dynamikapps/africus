//
//  WebViewController.swift
//  africUs
//
//  Created by Andy Metellus on 4/26/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit
import Alamofire

class WebViewController: UIViewController {

    // Class obj
    var link: String?
    var pageTitle: String?
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        componentAlignment()
        
        activityIndicator.startAnimating()

        if let openlink = link {

            getPage(url: openlink)
            
            if let title = pageTitle {
                self.navigationItem.title = title
            } else {
                self.navigationItem.title = "myafricus.com"
            }
        }
    }
    
    // Align webview UI
    func componentAlignment() {
        
        webView.frame = CGRect(x: 0,
                               y: 0,
                               width: self.view.frame.size.width,
                               height: self.view.frame.size.height - (self.navigationController!.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height))
        
        activityIndicator.center.x = webView.center.x
        activityIndicator.center.y = webView.center.y
    }
    
    // Get post information from site using WP API
    func getPage(url: String) {
        
        //Sending http post request
        Alamofire.request(url, method: .get, parameters: nil).responseJSON
            {
                response in
                //printing response
                print(response)
                print(response.result)
                
                switch (response.result) {
                case .success( _):
                    if let result = response.result.value
                    {
                        //converting it as NSDictionary
                        //let jsonData = result as? NSDictionary
                        
                        if ((result as? NSDictionary)?.value(forKey: "message") != nil)
                        {
                            // Cast json post data as NSDict
                            let jsonData = result as? NSDictionary
                            
                            let errorMessage = jsonData?.value(forKey: "message") as? String
                            
                            if let error = errorMessage {
                                
                                //MyGlobalClass.appDelegate.infoView(message: error, color: MyGlobalClass.redColor)
                                print(error)
                            }
                        }
                        else if ((result as? NSDictionary) != nil)
                        {
                            
                            let jsonData = result as? NSDictionary
                            
                            // Cast each post dict objct as a Post objct
                            if let page = jsonData {
                                
                                let pageObjct = Post(post: page)
                                
                                if let content = pageObjct.content {
                                    self.webView.loadHTMLString("\(MyGlobalClass.htmlStyleBegin)\(content)\(MyGlobalClass.htmlStyleEnd)", baseURL: nil)
                                    print("\(MyGlobalClass.htmlStyleBegin)\(content)\(MyGlobalClass.htmlStyleEnd)")
                                }
                            }
                        }
                    }
                case .failure( _):
                    
                    if let error = response.result.error as? URLError {
                        MyGlobalClass.appDelegate.infoView(message: error.localizedDescription, color: MyGlobalClass.redColor)
                    } else {
                        MyGlobalClass.appDelegate.infoView(message: "Something went wrong, try again later.", color: MyGlobalClass.redColor)
                    }
                    return
                }
        }
    }
}

extension WebViewController: UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
}
