//
//  MoreTableViewController.swift
//  africUs
//
//  Created by Handy Metellus on 1/10/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit

class MoreTableViewController: UITableViewController {
    
    // UI Obj
    @IBOutlet weak var suggestFact: UILabel!
    @IBOutlet weak var suggestImageView: UIImageView!
    @IBOutlet weak var shareApp: UILabel!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var privacy: UILabel!
    @IBOutlet weak var privacyImageView: UIImageView!
    @IBOutlet weak var terms: UILabel!
    @IBOutlet weak var termsImageView: UIImageView!
    @IBOutlet weak var attributions: UILabel!
    @IBOutlet weak var sourceImageView: UIImageView!
    @IBOutlet weak var version: UILabel!
    
    // App bundle info
    let appShortVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
    let navBackgroundImage = UIImage(named: "navBg")?.resizableImage(withCapInsets: UIEdgeInsetsMake(0, 15, 0, 15), resizingMode: UIImageResizingMode.stretch)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create nav bar shadow image
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Clear out line under nav bar
        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
        
        // Disable translucent
        self.navigationController?.navigationBar.isTranslucent = false
        
        // Remove back button title
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        // Align components
        componentAlignmentConfig()
        
        // App data
        version.text = "Version \(appShortVersion!).\(appVersion!)"
    }

    // Align components
    func componentAlignmentConfig(){
        
        let width = UIScreen.main.bounds.width
        
        // Suggest text and image
        suggestImageView.frame = CGRect(x: width / 25,
                                   y: width / 46.875,
                                   width: width / 15,
                                   height: width / 15)
        
        suggestFact.frame = CGRect(x: suggestImageView.frame.origin.x + suggestImageView.frame.size.height + (width / 25),
                                   y: 0,
                                   width: width / 1.2798,
                                   height: width / 8.52)
        
        // Share text and image
        shareImageView.frame = CGRect(x: width / 25,
                                        y: width / 46.875,
                                        width: width / 15,
                                        height: width / 15)
        
        shareApp.frame = CGRect(x: suggestImageView.frame.origin.x + suggestImageView.frame.size.height + (width / 25),
                                y: 0,
                                width: width / 1.2798,
                                height: width / 8.52)
        
        // Privacy text and image
        privacyImageView.frame = CGRect(x: width / 25,
                                      y: width / 46.875,
                                      width: width / 15,
                                      height: width / 15)
        
        privacy.frame = CGRect(x: suggestImageView.frame.origin.x + suggestImageView.frame.size.height + (width / 25),
                               y: 0,
                               width: width / 1.2798,
                               height: width / 8.52)
        
        // Terms text and image
        termsImageView.frame = CGRect(x: width / 25,
                                      y: width / 46.875,
                                      width: width / 15,
                                      height: width / 12.5)
        
        terms.frame = CGRect(x: suggestImageView.frame.origin.x + suggestImageView.frame.size.height + (width / 25),
                             y: 0,
                             width: width / 1.2798,
                             height: width / 8.52)
        
        // Att text and image
        sourceImageView.frame = CGRect(x: width / 25,
                                      y: width / 46.875,
                                      width: width / 15,
                                      height: width / 15)
        
        attributions.frame = CGRect(x: suggestImageView.frame.origin.x + suggestImageView.frame.size.height + (width / 25),
                                    y: 0,
                                    width: width / 1.2798,
                                    height: width / 8.52)
        
        // Version text
        version.frame = CGRect(x: width / 25,
                                   y: width / 46.875,
                                   width: width / 1.086,
                                   height: width / 8.52)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }
    
    // Check which section and rows number is selected
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch (indexPath.row) {
            
        //Suggestions or support touch
        case (0):
            //print("Suggestions or support touch.")
            if let storyboard = storyboard {
                let feedbackViewController = storyboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                let feedbackNav = UINavigationController(rootViewController: feedbackViewController)
                feedbackNav.isNavigationBarHidden = false
                feedbackNav.navigationBar.isTranslucent = false
                feedbackNav.navigationBar.barTintColor = MyGlobalClass.brandGreenColor
                feedbackNav.navigationBar.shadowImage = UIImage()
                feedbackNav.navigationBar.setBackgroundImage(navBackgroundImage, for: .default)
                feedbackNav.navigationBar.tintColor = UIColor.white
                feedbackNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                self.navigationController?.present(feedbackNav, animated: true, completion: nil)
            }
            break
        case (1):
            // Share tapped
            let firstActionItem = "Download the africUs app now from the App Store Store. #africus #history #facts #quotes https://itunes.apple.com/us/app/africus/id1200274542?mt=8"
            
            let actionViewController = UIActivityViewController(activityItems: [firstActionItem], applicationActivities: nil)
            self.present(actionViewController, animated: true, completion: nil)
            break
        case (2):
            //Privacy policy touch
            //print("Privacy policy touch.")
            if let storyboard = storyboard {
                let webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webViewController.link = "https://www.myafricus.com/wp-json/wp/v2/pages/79"
                webViewController.pageTitle = "Privacy Policy"
                navigationController?.pushViewController(webViewController, animated: true)
            }
            break
        case (3):
            //Terms of use touch
            //print("Terms of use touch.")
            if let storyboard = storyboard {
                let webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webViewController.link = "https://www.myafricus.com/wp-json/wp/v2/pages/81"
                webViewController.pageTitle = "Terms of Use"
                navigationController?.pushViewController(webViewController, animated: true)
            }
            break

        case (4):
            //Attributions touch
            //print("Attributions touch.")
            if let storyboard = storyboard {
                let attributionsViewController = storyboard.instantiateViewController(withIdentifier: "AttributionsViewController")
                navigationController?.pushViewController(attributionsViewController, animated: true)
            }
            break
        default:
            break
        }
    }
}
