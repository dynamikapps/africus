//
//  AttributionsViewController.swift
//  africUs
//
//  Created by Andy Metellus on 4/27/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Alamofire

class AttributionsViewController: UITableViewController {
    
    // Tableview user data from server
    var sourceResults: [Source] = []
    
    // Progress indicator
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Dynamic cell height
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 90
        
        // Align and config UI components
        componentAlignmentConfig()
        
        // Retrive all facts from db
        getSources()
    }
    
    // Align and config UI components
    func componentAlignmentConfig() {
        
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.color = MyGlobalClass.brandGreenColor
        tableView.addSubview(activityIndicator)
        
        activityIndicator.center.x = tableView.center.x
        activityIndicator.center.y = tableView.center.y
    }
    
    // Retrive all facts from db
    func getSources() {
        
        //let myUrl = "http://localhost/africus_app/scripts/sources.php"
        let myUrl = "https://myafricus.com/africus_app/scripts/sources.php"
        
        // Init Progress indicator
        activityIndicator.startAnimating()
        
        //Sending http post request
        Alamofire.request(myUrl, method: .post, parameters: nil).responseJSON
            {
                response in
                //printing response
                print(response)
                
                // Cleare old search data and reload table
                self.sourceResults.removeAll(keepingCapacity: false)
                self.tableView.reloadData()
                
                switch (response.result) {
                case .success( _):
                    if let result = response.result.value
                    {
                        //converting it as NSDictionary
                        let jsonData = result as? NSDictionary
                        
                        if (jsonData?.value(forKey: "sources") != nil)
                        {
                            // Add facts to dictionary array
                            let sources = jsonData?.value(forKey: "sources") as? [NSDictionary]
                            
                            if let sources = sources {
                                for source in sources {
                                    
                                    // Parse fact dictionary through fact obj
                                    let sourceResult = Source(source: source)
                                    self.sourceResults.append(sourceResult)
                                }
                                
                                self.tableView.reloadData()
                                // Stop activity indicator
                                self.activityIndicator.stopAnimating()
                                //print(self.sourceResults.count)
                                
                            }
                        }
                        else if (jsonData?.value(forKey: "message") != nil)
                        {
                            let errorMessage = jsonData?.value(forKey: "message") as? String
                            if let error = errorMessage {
                                MyGlobalClass.appDelegate.infoView(message: error, color: MyGlobalClass.redColor)
                            }
                        }
                    }
                case .failure( _):
                    
                    if let error = response.result.error as? URLError {
                        
                        MyGlobalClass.appDelegate.infoView(message: error.localizedDescription, color: MyGlobalClass.redColor)
                    } else {
                        
                        MyGlobalClass.appDelegate.infoView(message: "Something went wrong, could not connect.", color: MyGlobalClass.redColor)
                    }
                    return
                }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sourceResults.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SourceCell
        
        let source = sourceResults[indexPath.row]
        
        if let title = source.title {
            cell.titleLabel.text = title
        }
        
        if let author = source.author {
            cell.authorLabel.text = author
        }
        
        if let url = source.sourceURL {
            cell.urlTextView.text = url
        }
        
        return cell
    }
    
    // cell height
    /*override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }*/
}
