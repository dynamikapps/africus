//
//  FactModel.swift
//  africUs
//
//  Created by Handy Metellus on 5/24/16.
//  Copyright © 2016 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import Foundation
import UIKit
import GameKit

// Fact class
class Fact: NSObject {
    
    var fact_id: String!
    var fact: String!
    var createdDate: String!
    
    // Class var init
    init(fact: NSDictionary) {
        
        self.fact_id = String(describing: fact.value(forKey: "fact_id") as! Int)
        self.fact = fact.value(forKey: "fact") as? String
        self.createdDate = fact.value(forKey: "created_date") as? String
    }
}

// Define protocols that get random fact, must have facts array and return random facts.

protocol FactModelType {
    var facts: [String] { get set }
    func getRandomFact() -> String
}

protocol ColorModelType {
    var colors: [UIColor] { get }
    func getRandomColor() -> UIColor
}

// Color Enum Object

enum ColorComponents {
    case rgb(red: CGFloat, green: CGFloat,blue: CGFloat, alpha: CGFloat)
    case hsb(CGFloat, CGFloat, CGFloat, CGFloat)
    
    func color() -> UIColor {
        switch self {
        case .rgb(let redValue, let greenValue, let blueValue, let alphaValue):
            return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alphaValue)
        case .hsb(let hueValue, let saturationValue, let brightnessValue, let alphaValue):
            return UIColor(hue: hueValue/360, saturation: saturationValue/100, brightness: brightnessValue/100.0, alpha: alphaValue)
        }
    }
}

// Error Types

enum FactsError: Error {
    case invalidResource
    case conversionError
    //case InvalidKey
}

// Example ColorComponents.RGB(red: 61.0, green: 120.0, blue: 198.0, alpha: 1.0).color()
// Example ColorComponents.HSB(61.0, 120.0, 198.0, 1.0).color()

struct ColorModel: ColorModelType {
    // Array can hold anything of the same type
    let colors = [ColorComponents.rgb(red: 90.0, green: 187.0, blue: 181.0, alpha: 1.0).color(), //aqua green
                  //ColorComponents.rgb(red: 223.0, green: 86.0, blue: 94.0, alpha: 1.0).color(), //pink
                  //ColorComponents.rgb(red: 239.0, green: 130.0, blue: 100.0, alpha: 1.0).color(), // pink orange
                  ColorComponents.rgb(red: 77.0, green: 75.0, blue: 82.0, alpha: 1.0).color(), //gray
                  ColorComponents.rgb(red: 105.0, green: 94.0, blue: 133.0, alpha: 1.0).color(), //gray blue
                  ColorComponents.rgb(red: 85.0, green: 176.0, blue: 112.0, alpha: 1.0).color(), //green
                  ColorComponents.rgb(red: 54.0, green: 0.0, blue: 218.0, alpha: 1.0).color(), //blue
                  ColorComponents.rgb(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0).color(), // black
                  ColorComponents.rgb(red: 204.0, green: 51.0, blue: 0.0, alpha: 1.0).color(), //red orange
                  ColorComponents.rgb(red: 153.0, green: 153.0, blue: 102.0, alpha: 1.0).color(), //olive green
                  ColorComponents.rgb(red: 204.0, green: 51.0, blue: 255.0, alpha: 1.0).color(), //bright purple
                  ColorComponents.rgb(red: 255.0, green: 0.0, blue: 102.0, alpha: 1.0).color(), //darker pink
                  ColorComponents.rgb(red: 153.0, green: 0.0, blue: 204.0, alpha: 1.0).color(), //purple
                  ColorComponents.rgb(red: 255.0, green: 0.0, blue: 0.0, alpha: 1.0).color(), //red
                  ColorComponents.rgb(red: 0.0, green: 153.0, blue: 0.0, alpha: 1.0).color(), //green
                  ColorComponents.rgb(red: 0.0, green: 122.0, blue: 77.0, alpha: 1.0).color(), //blue green
                  ColorComponents.rgb(red: 133.0, green: 133.0, blue: 173.0, alpha: 1.0).color(), //gray blue
                  ColorComponents.rgb(red: 153.0, green: 153.0, blue: 255.0, alpha: 1.0).color(), //teal blue
                  ColorComponents.rgb(red: 0.0, green: 153.0, blue: 204.0, alpha: 1.0).color(), // aqua blue
                  ColorComponents.rgb(red: 204.0, green: 0.0, blue: 204.0, alpha: 1.0).color(), //light purple
                  ColorComponents.rgb(red: 102.0, green: 51.0, blue: 0.0, alpha: 1.0).color(), //brown
                  ColorComponents.rgb(red: 153.0, green: 51.0, blue: 51.0, alpha: 1.0).color(), //red brown
                  ColorComponents.rgb(red: 128.0, green: 0.0, blue: 0.0, alpha: 1.0).color(), // brown red
                  ColorComponents.rgb(red: 153.0, green: 102.0, blue: 51.0, alpha: 1.0).color() //brown gold
    ]
    
    func getRandomColor() -> UIColor {
        // Create a random number from gamekit random number, max rand is array max number
        let randomNumber = GKRandomSource.sharedRandom().nextInt(upperBound: colors.count)
        return colors[randomNumber]
    }
}

// Helper Classes
// Class Methods for Class or Static Methods for Structs - Does not need an instance created to called method. Just call call name.
// Type Method

class PlistConverter {
    
    class func dictionaryFromFile(_ resource: String, ofType type: String) throws -> [String] {
        // Finds the plist file in the directory bundle
        guard let path = Bundle.main.path(forResource: resource, ofType: type) else {
            throw FactsError.invalidResource
        }
        // Retrieve content and convert to array, full initialized
        guard let factsList = NSArray(contentsOfFile: path),
            let castFactsList = factsList as? [String] else {
                throw FactsError.conversionError
        }
        return castFactsList
    }
}

// Store facts from plist file
struct FactModel: FactModelType {
    var facts: [String]
    
    func getRandomFact() -> String {
        // Create a random number from gamekit random number, max rand is array max number
        let randomNumber = GKRandomSource.sharedRandom().nextInt(upperBound: facts.count)
        return facts[randomNumber]
    }
}

// Get Random bg color image
struct BgImageModel {
    
    let images = [UIImage(named: "factBgBlack")!,
                  UIImage(named: "factBgBlue")!,
                  UIImage(named: "factBgRed")!,
                  UIImage(named: "factBgOrange")!,
                  UIImage(named: "factBgGray")!,
                  UIImage(named: "factBgPurple")!,
                  UIImage(named: "factBgGreen")!,
                  UIImage(named: "factBgTeal")!,
                  UIImage(named: "factBgLightGreen")!
    ]
    
    func getRandomImage() -> UIImage {
        // Create a random number from gamekit random number, max rand is array max number
        let randomNumber = GKRandomSource.sharedRandom().nextInt(upperBound: images.count)
        return images[randomNumber]
    }
}

