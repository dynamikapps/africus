//
//  SourceModel.swift
//  africUs
//
//  Created by Andy Metellus on 5/4/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import Foundation

// Fact class
class Source: NSObject {
    
    var source_id: String!
    var title: String!
    var author: String!
    var sourceURL: String!
    var sourceImagePath: String!
    var createdDate: String!
    
    // Class var init
    init(source: NSDictionary) {
        
        self.source_id = String(describing: source.value(forKey: "source_id") as! Int)
        self.author = source.value(forKey: "author") as? String
        self.title = source.value(forKey: "title") as? String
        self.sourceURL = source.value(forKey: "source_url") as? String
        self.sourceImagePath = source.value(forKey: "source_image_path") as? String
        self.createdDate = source.value(forKey: "created_date") as? String
    }
}
