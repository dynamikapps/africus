 //
 //  AppDelegate.swift
 //  africUs
 //
 //  Created by Handy Metellus on 5/23/16.
 //  Copyright © 2016 NJ VAUGHN MEDIA, LLC. All rights reserved.
 //
 
 import UIKit
 import Firebase
 import UserNotifications
 import FirebaseInstanceID
 import FirebaseMessaging
 import Alamofire
 
 @UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    // Check if application notification settings
    var pushBadge: String?
    var pushAlert: String?
    var pushSound: String?
    
    // Bool to check if errorView is currently showing
    var infoViewIsShowing = false
    
    override init() {
        FirebaseApp.configure()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Customize navigation bar colors
        
        let navigationBarAppearace = UINavigationBar.appearance()
        //navigationBarAppearace.shadowImage = UIImage()
        //navigationBarAppearace.setBackgroundImage(UIImage(), for: .default)
        navigationBarAppearace.tintColor = UIColor.white
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        UIApplication.shared.isStatusBarHidden = false
        UITabBar.appearance().isTranslucent = false
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            // For easy APNs Notifictions
            UNUserNotificationCenter.current().getNotificationSettings(){ (setttings) in
                
                switch setttings.badgeSetting {
                case .enabled:
                    
                    print("enabled badge setting")
                    self.pushBadge = "enabled"
                    
                case .disabled:
                    
                    print("setting has been disabled")
                    self.pushBadge = "disabled"
                    
                case .notSupported:
                    print("something vital went wrong here")
                    self.pushBadge = "disabled"
                }
                
                
                switch setttings.alertSetting {
                case .enabled:
                    
                    print("enabled alert setting")
                    self.pushAlert = "enabled"
                    
                case .disabled:
                    
                    print("setting has been disabled")
                    self.pushAlert = "disabled"
                    
                case .notSupported:
                    print("something vital went wrong here")
                    self.pushAlert = "disabled"
                }
                
                
                switch setttings.soundSetting {
                case .enabled:
                    
                    print("enabled sound setting")
                    self.pushSound = "enabled"
                    
                case .disabled:
                    
                    print("setting has been disabled")
                    self.pushSound = "disabled"
                    
                case .notSupported:
                    print("something vital went wrong here")
                    self.pushSound = "disabled"
                }
            }
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            
            pushBadge = settings.types.contains(.badge) ? "enabled" : "disabled"
            pushAlert = settings.types.contains(.alert) ? "enabled" : "disabled"
            pushSound = settings.types.contains(.sound) ? "enabled" : "disabled"
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        return true
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full APNs notification message.
        print("Message details: \(userInfo)")
        
        /*if let aps = userInfo["aps"] as? NSDictionary
         {
         if let alertMessage = aps["alert"] as? String {
         
         self.infoView(message: alertMessage, color: MyGlobalClass.greenColor)
         }
         
         }*/
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full APNs notification message.
        print("Message details: \(userInfo)")
        
        /*if let aps = userInfo["aps"] as? NSDictionary
         {
         if let alertMessage = aps["alert"] as? String {
         
         self.infoView(message: alertMessage, color: MyGlobalClass.greenColor)
         }
         
         }*/
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    // Info view on top of vc
    func infoView(message: String, color: UIColor) {
        if infoViewIsShowing == false {
            
            // Cast that info view is currently showing
            infoViewIsShowing = true
            
            // Info view - red backgound view
            let infoViewHeight = self.window!.bounds.height / 14.2
            let infoViewY = 0 - infoViewHeight
            let smoothRedColor = color
            
            // Create info view and add to main view
            let infoView = UIView(frame: CGRect(origin: CGPoint(x: 0,y :infoViewY), size: CGSize(width: self.window!.bounds.width, height: infoViewHeight)))
            infoView.backgroundColor = smoothRedColor
            self.window!.addSubview(infoView)
            
            // Errorlabel - error view label text
            let infoLabelWidth = infoView.bounds.width
            let infoLabelHeight = infoView.bounds.height + UIApplication.shared.statusBarFrame.height / 2
            
            let infoLabel = UILabel()
            infoLabel.frame.size.width = infoLabelWidth
            infoLabel.frame.size.height = infoLabelHeight
            infoLabel.numberOfLines = 0
            
            infoLabel.text = message
            print("print this message -------> \(message)")
            infoLabel.font = UIFont(name: "HelveticaNeue", size: MyGlobalClass.fontSize12)
            infoLabel.textColor = .white
            infoLabel.textAlignment = .center
            
            infoView.addSubview(infoLabel)
            
            // Animate error view
            
            UIView.animate(withDuration: 0.5, animations: {
                
                // Move down errorview
                infoView.frame.origin.y = 0
                
                // If animation did finish
            }, completion: { (finished: Bool) in
                
                // If finished is true
                if finished == true {
                    
                    UIView.animate(withDuration: 0.5, delay: 2.00, options: .curveLinear, animations: {
                        
                        // Move up errorview
                        infoView.frame.origin.y = infoViewY
                        
                        // If all animation finished
                    }, completion: { (finished: Bool) in
                        
                        if finished {
                            infoView.removeFromSuperview()
                            infoLabel.removeFromSuperview()
                            self.infoViewIsShowing = false
                        }
                    })
                }
            })
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        //InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.prod)
        
        //send this device token to server
        
        // Prepare device token to have a proper format
        let deviceTokenString = deviceToken.hexString
        
        //print("Got token data! \(deviceTokenString)")
        
        //print("push badge: " + pushBadge!)
        //print("push alert: " + pushAlert!)
        //print("push sound: " + pushSound!)
        
        let myDevice = UIDevice();
        let deviceName = myDevice.name
        let deviceModel = myDevice.model
        let systemVersion = myDevice.systemVersion
        let deviceId = myDevice.identifierForVendor!.uuidString
        
        var appName:String?
        if let appDisplayName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName")
        {
            appName = appDisplayName as? String
        } else {
            appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String
        }
        
        let shortVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let buildVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
        
        let appVersion = "\(shortVersion!).\(buildVersion!)"
        
        let myUrl = "https://myafricus.com/africus_app/apns/apns.php"
        //let myUrl = "http://localhost/africus_pp/apns/apns.php"
        
        
        //creating parameters for the post request
        let parameters: Parameters = [
            "task":"register",
            "appname":appName!,
            "appversion":appVersion,
            "deviceuid":deviceId,
            "devicetoken":deviceTokenString,
            "devicename":deviceName,
            "devicemodel":deviceModel,
            "deviceversion":systemVersion,
            "pushbadge":pushBadge!,
            "pushalert":pushAlert!,
            "pushsound":pushSound!
        ]
        
        //print("\(parameters)")
        //Sending http post request
        Alamofire.request(myUrl, method: .post, parameters: parameters).responseJSON
            {
                response in
                //printing response
                //print(response)
                switch (response.result) {
                case .success( _):
                    if let result = response.result.value
                    {
                        print("response \(result)")
                        
                    }
                case .failure( _):
                    
                    if let error = response.result.error as? URLError {
                        print("Something went wrong! \(error.localizedDescription)")
                    }
                    return
                }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    // [END disconnect_from_fcm]
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    // [START connect_on_active]
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    // [END connect_on_active]
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    /*func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
     print("Unable to register for remote notifications: \(error.localizedDescription)")
     }*/
    
    /*func userIsLoggedIn() {
     self.window = UIWindow(frame: UIScreen.main.bounds)
     let userId = UserDefaults.standard.string(forKey: "userId")
     let emailVerified = UserDefaults.standard.bool(forKey: "emailVerified")
     if userId != nil && emailVerified {
     //Navigate to Reveal View Controller
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let VC = storyboard.instantiateViewController(withIdentifier: "FactNavController") as! UINavigationController
     self.window!.rootViewController = VC
     } else {
     let storyboard = UIStoryboard(name: "Main", bundle: nil)
     let VC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as UIViewController
     self.window!.rootViewController = VC
     }
     self.window!.makeKeyAndVisible()
     }*/
 }
 
 // [START ios_10_message_handling]
 @available(iOS 10, *)
 extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Easy apns app notification print full message.
        print("Message details: \(userInfo)")
        
        /*if let aps = userInfo["aps"] as? NSDictionary
         {
         if let alertMessage = aps["alert"] as? String {
         
         self.infoView(message: alertMessage, color: MyGlobalClass.greenColor)
         }
         }*/
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Easy apns app notification
        print("Message details: \(userInfo)")
        
        /*if let aps = userInfo["aps"] as? NSDictionary
         {
         if let alertMessage = aps["alert"] as? String {
         
         self.infoView(message: alertMessage, color: MyGlobalClass.greenColor)
         }
         }*/
        
        completionHandler()
    }
 }
 // [END ios_10_message_handling]
 // [START ios_10_data_message_handling]
 extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
 }
 // [END ios_10_data_message_handling]
 
 extension Data {
    var hexString: String {
        return map { String(format: "%02.2hhx", arguments: [$0]) }.joined()
    }
 }
 
